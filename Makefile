.PHONY: readme test

before-publish: test readme doc
	@ git status

test:
	@ cargo test --features terminfo
	@ cargo test

doc:
	@ cargo doc --features terminfo
	@ cargo doc

docpriv:
	@ cargo doc --features terminfo --document-private-items
	@ cargo doc --document-private-items

readme: README.md color-print/README.md

README.md: color-print/src/lib.rs
	@ cd color-print; cargo readme > ../README.md
	@ sed -i 's/\[\(`[^`]*`\)]/\1/g' README.md

color-print/README.md: README.md
	@ cp README.md color-print
